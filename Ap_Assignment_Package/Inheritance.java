package Ap_Assignment_Package;
class add{  //super , parent
    add(){

    }
    add(int a , int b){
        System.out.println(a+b);
    }
    void run(){
        System.out.println("this is add class");
    }
}
class sub extends add { //sub , child
    void run2(){
        System.out.println("this is sub class");
    }
}
class mul extends add  { //multi level , multiple a-b-c ,normal a-b , hier a-b , a-c
    //add , sub
}
public class Inheritance {
    public static void main(String[] args) {
        System.out.println("inheritance");
        mul s = new mul();
        s.run();
        //s.run2();
    }
}
