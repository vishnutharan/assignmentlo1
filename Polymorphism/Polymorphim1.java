package Polymorphism;
class Shape{
	public double getarea()
	{ 		return 0; 		}
	public String getparameter() {
		return null;	}
	}
class Rectangle extends Shape{
	private double width;
	private double length;
	public Rectangle(double w,double l)
	{ 	this.width=w;
		this.length=l;	}
		public double getarea() {
		return width*length;	}
}
class circle extends Shape{
	private double radion;
	public circle(double r)
	{
		this.radion=r;	}
		public double getarea()
	{			return 3.14*radion*radion; 		}
}
public class Polymorphim1 {
	public static void main(String[] args) {
		Shape[] shapes=new Shape[2];
		shapes[0]=new circle(7);
		shapes[1]=new Rectangle(4,6);
		System.out.println("Area of circle:"+shapes[0].getarea());
		System.out.println("Area of Rectangle:"+shapes[1].getarea());
	}
}

