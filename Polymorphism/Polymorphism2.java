package Polymorphism;

public class Polymorphism2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Shape [] shapes=new Shape[2];
		shapes[0]=new circle(7);
		shapes[1]=new Rectangle(4,6);
		System.out.println("Area of circle:"+shapes[0].getarea());
		System.out.println("Area of Rectangle:"+shapes[1].getarea());
		System.out.println("parameter of circle:"+shapes[0].getparameter());
		System.out.println("parameter of Rectangle:"+shapes[1].getparameter());

	}

}
