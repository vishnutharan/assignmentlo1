package Abstraction;

public class Student extends Person {

	private int stuId;

	public Student(String name, String gender, int stuId) {
		super(name, gender);
		this.stuId = stuId;
	}

	@Override
	public void study() {
		if (stuId == 0) {
			System.out.println("Not working!!!");
		} else {
			System.out.println("Studying as a Student!!!!!!");
		}
	}
}
